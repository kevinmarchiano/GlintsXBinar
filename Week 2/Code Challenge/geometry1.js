// Import readline
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function to calculate beam volume
function beam(length, width, height) {
  return length * width * height;
}

// All input just in one code
function input() {
    rl.question('Length: ', function (length) {
      rl.question('Width: ', (width) => {
        rl.question('Height: ', (height) => {
          if (length > 0 && width > 0 && height > 0) {
            console.log(`\nBeam: ${beam(length, width, height)}`);
            rl.close();
          } else {
            console.log(`Length, Width and Height must be a number\n`);
            input();
          }
        });
      });
    });
  }
  
  console.log(`Rectangle`);
  console.log(`=========`);
  input(); 
  