/* If we use callback function, we can't add another logic outside the function */

// Readline
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function to calculate cube volume
function cube(side) {
  return side * side * side;
}

// Function for inputing side of cube
function inputSide() {
  rl.question(`Side: `, (side) => {
    if (!isNaN(side)) {
      console.log('Cube Volume: ' + side * side * side)
      rl.close();
    } else {
      console.log(`Side must be a number\n`);
      inputSide();
    }
  });
}

console.log(`Cube`);
console.log(`=========`);
inputSide(); 
