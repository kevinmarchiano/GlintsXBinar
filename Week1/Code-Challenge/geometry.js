// cube argument
function cubegemoetry(side) {
    console.log(side ** 3);
}
cubegemoetry(30);
cubegemoetry(40);

// cube return
function cube(side) {
    console.log('cube:' + side ** 3);
    return side * side * side;
}

let cubeOne = cube(10);
let cubeTwo = cube(20);
console.log('CubeOne + CubeTwo:' +(cubeOne + cubeTwo));

// beam argument
function beamgeometry(width,length, height){
    console.log(width * length * height)
}

beamgeometry(10,20,30);
beamgeometry(20,40,50);
// beam return
function beam(width,length, height) {
    console.log('beam:' + width * length * height);
    return width * length * height
}

let beamOne = beam(20,30,40);
let beamTwo = beam(10,20,30);
console.log('beamOne + beamTwo:' + (beamOne * beamTwo));