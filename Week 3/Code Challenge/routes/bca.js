const express = require('express'); 

const { getBCABranch } = require('../controller/bca');

const router = express.Router(); 

router.get('/', getBCABranch);

module.exports = router;